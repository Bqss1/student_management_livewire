<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('students', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('nama_lengkap', 244);
            $table->string('nim', 15)->unique();
            $table->enum('jenis_kelamin', ['L', 'P']);
            $table->string("tempat_lahir", 100);
            $table->date('tanggal_lahir');
            $table->string("email", 100)->unique();
            $table->string('nomor_telepon', 15);
            $table->text('alamat');
            $table->string("foto_profil", 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('students');
    }
};
