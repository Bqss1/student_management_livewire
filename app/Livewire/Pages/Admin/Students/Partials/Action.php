<?php

namespace App\Livewire\Pages\Admin\Students\Partials;

use App\Livewire\Pages\Admin\Students\EditStudentModal;
use App\Models\StudentModel;
use Illuminate\Support\Facades\Storage;
use Livewire\Attributes\On;
use Livewire\Component;

class Action extends Component
{
    public $student;
    public function mount($student)
    {
        $this->student = $student;
    }

    #[On('delete')]
    public function delete(StudentModel $student)
    {
        try{
            Storage::delete($this->student->foto_profil);
            $student->delete();
            $this->dispatch('student-deleted')->to(EditStudentModal::class);
        }catch(\Exception $e){
            $this->dispatch('swal', [
                'title' => 'Gagal',
                'text' => 'Gagal menghapus data',
                'type' => 'error'
            ]);
        }
    
    }

    public function render()
    {
        return view('livewire.pages.admin.students.partials.action');
    }
}
