<?php

namespace App\Livewire\Pages\Admin\Students;

use App\Livewire\Forms\Admin\StudentForm;
use Livewire\Attributes\On;
use App\Models\StudentModel;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditStudentModal extends Component
{
    use WithFileUploads;
    public StudentForm $form;

  

    #[On('edit')]
    public function edit(StudentModel $student){
        if($student){

            $this->form->fillForm($student);

            $this->dispatch("student-edit-modal-show");
        }else{
            $this->dispatch("swal",[
                'livewire_intance' => $this->getId(),
                'type' => "Error",
                'text' => 'Failed to load student. Please try again later.',
            ]);
        }
    }
    #[On('removeFotoProfil')]
    public function removeFotoProfil(){
        dd("remove");
        $this->form->foto_profil = null;
    }

    public function update(){
        try {
            $this->form->update();
            $this->dispatch("student-updated");
        } catch (\Exception $e) {
            $this->dispatch("swal",[
                'livewire_intance' => $this->getId(),
                'type' => "error",
                'text' => 'Failed to update student, '.$e->getMessage(),
            ]);
        }
    }
    #[On("reset")]
    public function resetForm(){
        $this->form->reset();
        $this->resetValidation();
    }

    public function render()
    {
        return view('livewire.pages.admin.students.edit-student-modal');
    }
}
