<?php

namespace App\Livewire\Pages\Admin\Students;

use App\Livewire\Forms\Admin\StudentForm;
use Livewire\Attributes\On;
use Livewire\Component;
use Livewire\WithFileUploads;

class AddStudentModal extends Component
{
    use WithFileUploads;
    public StudentForm  $form;

    public function render()
    {
        return view('livewire.pages.admin.students.add-student-modal');
    }

    public function store()
    {
        if ($this->form->store()) {
            $this->dispatch("student-added");
            return;
        }else{
            $this->dispatch("swal", [
                'livewire_intance' => $this->getId(),
                'type' => "error",
                'text' => 'Failed to add student. Please try again later.',
            ]);
        }
    }

    #[On("reset")]
    public function resetForm()
    {
        $this->form->reset();
        $this->form->resetValidation();
    }
}
