<?php

namespace App\Livewire\Forms\Admin;

use App\Models\StudentModel;
use Illuminate\Support\Facades\Storage;
use Livewire\Form;
use Livewire\WithFileUploads;

class StudentForm extends Form
{

    use WithFileUploads;
    public $id;
    public $nama_lengkap;
    public $nim;
    public $jenis_kelamin;
    public $tempat_lahir;
    public $tanggal_lahir;
    public $email;
    public $nomor_telepon;
    public $alamat;
    public $foto_profil;

    public function rules(): array
    {
        return [
            'nama_lengkap' => 'required',
            'nim' => 'required|unique:students,nim,' . $this->id,
            'jenis_kelamin' => 'required|in:L,P',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'email' => 'required|email',
            'nomor_telepon' => 'required|numeric|digits_between:10,13|unique:students,nomor_telepon,' . $this->id,
            'alamat' => 'required',
            'foto_profil' => 'required|image|max:1024',
        ];
    }

    public function store()
    {
        $this->validate();
        try {
            $foto_profil = Storage::disk("public")->put("foto_profil", $this->foto_profil);
            $student = StudentModel::create([
                ...$this->except("foto_profil"),
                "foto_profil" => $foto_profil
            ]);
            return $student;
        } catch (\Exception $e) {
            Storage::delete($foto_profil);
            return false;
        }


        return $student;
    }

    public function fillForm(StudentModel $student)
    {
        $this->id = $student->id;
        $this->nama_lengkap = $student->nama_lengkap;
        $this->nim = $student->nim;
        $this->jenis_kelamin = $student->jenis_kelamin;
        $this->tempat_lahir = $student->tempat_lahir;
        $this->tanggal_lahir = $student->tanggal_lahir;
        $this->email = $student->email;
        $this->nomor_telepon = $student->nomor_telepon;
        $this->alamat = $student->alamat;
        $this->foto_profil = Storage::url($student->foto_profil);
    }

    public function update()
    {
        if (gettype($this->foto_profil) == "string") {
            $this->validate([
                'nama_lengkap' => 'required',
                'nim' => 'required|unique:students,nim,' . $this->id,
                'jenis_kelamin' => 'required|in:L,P',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required|date',
                'email' => 'required|email',
                'nomor_telepon' => 'required|numeric|digits_between:10,13|unique:students,nomor_telepon,' . $this->id,
                'alamat' => 'required',
            ]);
            $student = StudentModel::find($this->id);
            if (!$student) {
                throw new \Exception("Student not found", 404);
            }
            $student->update($this->except("foto_profil"));
        } else {
            $this->validate();
            $foto_profil = Storage::disk("public")->put("foto_profil", $this->foto_profil);
            $student = StudentModel::find($this->id);
            if (!$student) {
                throw new \Exception("Student not found", 404);
            }
            Storage::delete($student->foto_profil);
            $student->update([
                ...$this->except("foto_profil"),
                "foto_profil" => $foto_profil
            ]);
        }
    }
}
