<?php

namespace App\DataTables\admin;

use App\Models\Student;
use App\Models\StudentModel;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\Storage;
use Livewire\Livewire;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class StudentDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addIndexColumn()
            ->addColumn('action', function (StudentModel $val) {
                return Livewire::mount('pages.admin.students.partials.action', ['student' => $val]);
            })
            ->editColumn('foto_profil', function (StudentModel $val) {
                return '<img src="' . Storage::url($val->foto_profil) . '" class="img-fluid object-fit-cover" alt="profil" width="40" heigh="40">';
            })
            ->addColumn("ttl", function (StudentModel $val) {
                return ucfirst($val->tempat_lahir) . ", " . $val->tanggal_lahir;
            })
            ->rawColumns(['action',"foto_profil"])
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(): QueryBuilder
    {
        return StudentModel::orderBy('created_at', 'desc');
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('students-table')
            ->columns($this->getColumns())
            ->minifiedAjax(script: "
            data._token = '" . csrf_token() . "';
            data._p = 'POST';
        ")
            ->dom('rt' . "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>",)
            ->addTableClass('table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer text-gray-600 fw-semibold')
            ->setTableHeadClass('text-start text-muted fw-bold fs-7 text-uppercase gs-0')
            ->orderBy(2)
            ->drawCallbackWithLivewire(file_get_contents(public_path('assets/js/custom/table/_init.js')))
            ->select(false)
            ->buttons([]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::computed("DT_RowIndex")
                ->title("No.")
                ->width(20),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            Column::make("foto_profil"),
            Column::make("nama_lengkap"),
            Column::make("jenis_kelamin")->title("Gender"),
            Column::make("ttl")->title("Tempat/Tanggal Lahir"),
            Column::make("email"),
            Column::make("nomor_telepon"),

        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Student_' . date('YmdHis');
    }
}
