<?php

namespace App\Http\Controllers;

use App\DataTables\admin\StudentDataTable;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(StudentDataTable $datatable, Request $request)
    {
        return $datatable->render('pages.admin.students.index');
    }

}
