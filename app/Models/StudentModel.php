<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentModel extends Model
{
    use HasFactory, HasUuids;

    protected $table = "students";

    protected $guarded = [
        "id",
        "created_at",
        "updated_at",
    ];
}
