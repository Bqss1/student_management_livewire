<div class="d-flex justify-content-center align-items-center gap-2">
  <button wire:click="$dispatchTo('pages.admin.students.edit-student-modal','edit', {student:'{{ $student->id }}'})"
    class="btn btn-light btn-active-light-primary p-3 btn-center btn-sm">
    <i class="ki-outline ki-pencil fs-2"></i>
  </button>
  <button data-action-params='{{ json_encode(['student' => $student->id]) }}' data-livewire-instance="@this"
    data-action="delete" data-action-delete class="btn btn-light btn-active-light-primary p-3 btn-center btn-sm">
    <i class="ki-outline ki-trash fs-2"></i>
  </button>
</div>

