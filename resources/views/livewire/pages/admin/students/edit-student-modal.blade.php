<div>
  <x-mollecules.modal size="lg" id="edit-student_modal" action="update" wire:ignore.self>
    <x-slot:title>Edit Student</x-slot:title>
    <div class="">
      <div class="mb-6">
        <x-atoms.form-label required class="d-block">Foto Profil</x-atoms.form-label>
        <div
          class="ms-6 mt-4 image-input image-input-outline image-input-placeholder @if (!$form->foto_profil) image-input-empty @endif   @error('form.foto_profil')
          border border-1 border-danger
        @enderror"
          id="profile_field_edit" data-kt-image-input="true">
          <!--begin::Image preview wrapper-->
          @if ($form->foto_profil)
            @if (gettype($form->foto_profil) == 'string')
              <div class="image-input-wrapper w-125px h-125px" style="background-image:url('{{ $form->foto_profil }}');"
                id="profile_field_edit_wrapper">
              </div>
            @else
              <div class="image-input-wrapper w-125px h-125px"
                style="background-image:url('{{ $form->foto_profil->temporaryUrl() }}');"
                id="profile_field_edit_wrapper">
              </div>
            @endif
          @else
            <div class="image-input-wrapper w-125px h-125px" id="profile_field_edit_wrapper">
            </div>
          @endif
          <!--end::Image preview wrapper-->

          <!--begin::Edit button-->
          <label class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
            data-kt-image-input-action="change" data-bs-toggle="tooltip" data-bs-dismiss="click" title="Change avatar">
            <i class="ki-duotone ki-pencil fs-6"><span class="path1"></span><span class="path2"></span></i>

            <!--begin::Inputs-->
            <input type="file" name="foto_profil" wire:model="form.foto_profil" accept=".png, .jpg, .jpeg" />
            <input type="hidden" name="avatar_remove" />
            <!--end::Inputs-->
          </label>
          <!--end::Edit button-->

          <!--begin::Cancel button-->
          <span class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
            data-kt-image-input-action="cancel" data-bs-toggle="tooltip" data-bs-dismiss="click" title="Cancel avatar">
            <i class="ki-outline ki-cross fs-3"></i>
          </span>
          <!--end::Cancel button-->

          <!--begin::Remove button-->
          <span class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
            data-kt-image-input-action="remove" data-bs-toggle="tooltip" data-bs-dismiss="click" title="Remove avatar">
            <i class="ki-outline ki-cross fs-3"></i>
          </span>
          <!--end::Remove button-->
        </div>
        @error('form.foto_profil')
          <small class="text-danger d-block mt-2">{{ $message }}</small>
        @enderror
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Nama Lengkap</x-atoms.form-label>
        <x-atoms.input name="nama_lengkap" wire:model="form.nama_lengkap" />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>NIM</x-atoms.form-label>
        <x-atoms.input name="nim" wire:model="form.nim" />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Jenis Kelamin</x-atoms.form-label>
        <x-atoms.select name="jenis_kelamin" wire:model="form.jenis_kelamin">
          <option value="">Pilih Jenis Kelamin</option>
          <option value="L">Laki-Laki</option>
          <option value="P">Perempuan</option>
        </x-atoms.select>
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Tempat Lahir</x-atoms.form-label>
        <x-atoms.input name="tempat_lahir" wire:model='form.tempat_lahir' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Tanggal Lahir</x-atoms.form-label>
        <x-atoms.input type="date" name="tanggal_lahir" wire:model="form.tanggal_lahir" />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Email</x-atoms.form-label>
        <x-atoms.input name="email" type="email" wire:model='form.email' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Nomor Telepon</x-atoms.form-label>
        <x-atoms.input name="nomor_telepon" type="tel" wire:model='form.nomor_telepon' />
      </div>
      <div class="mb-6">
        <x-atoms.form-label required>Alamat Lengkap</x-atoms.form-label>
        <x-atoms.textarea name="alamat" wire:model="form.alamat"></x-atoms.textarea>
      </div>
      <x-slot:footer>
        <x-atoms.button class="btn-primary btn" type="submit" action="update">Update</x-atoms.button>
      </x-slot:footer>
    </div>
  </x-mollecules.modal>

</div>

@push('css')
  <style>
    .image-input-placeholder {
      background-image: url('/assets/media/svg/avatars/blank.svg');
    }

    [data-bs-theme="dark"] .image-input-placeholder {
      background-image: url('/assets/media/svg/avatars/blank-dark.svg');
    }
  </style>
@endpush

@push('scripts')
  <script>
    document.addEventListener('livewire:initialized', () => {

      const imageInputElement = document.querySelector("#profile_field_edit");
      // console.log(KTImageInput);
      // const imageInput = KTImageInput.getInstance(imageInputElement);
      // imageInput.on("kt.imageinput.remove", function() {
      //   @this.dispatch("removeFotoProfil")
      // });

      function refreshTable() {
        window.LaravelDataTables['students-table'].ajax.reload();
      };

      @this.on("student-updated", () => {
        $('#edit-student_modal').modal('hide');
        Swal.fire({
          title: 'Success!',
          text: 'Student has been updated',
          icon: 'success',
          confirmButtonText: 'Okay',
        });
        refreshTable();
      });

      @this.on("student-deleted", () => {
        Swal.fire({
          title: 'Success!',
          text: 'Student has been deleted',
          icon: 'success',
          confirmButtonText: 'Close'
        }).then(() => {
          window.LaravelDataTables['students-table'].ajax.reload();
        })
      })



      @this.on("student-edit-modal-show", () => {
        console.log("open bang");
        $('#edit-student_modal').modal('show');
      });

    });
  </script>
@endpush
